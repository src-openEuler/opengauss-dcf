Name:             DCF
Version:          6.0.0
Release:          10
Summary:          A distributed consensus framework library
License:          MulanPSL-2.0
URL:              https://gitee.com/opengauss/DCF
Source0:          %{name}-%{version}.tar.gz

Patch1:           DCF-6.0.0-sw.patch
Patch2:           DCF-6.0.0-add-riscv64-support.patch

BuildRequires: tar cmake gcc gcc-c++ lz4-devel openssl-devel zstd-devel libboundscheck cjson-devel


%description
DCF is A distributed consensus framework library for openGauss


%prep
%setup -q
%ifarch sw_64
%patch -P1 -p1
%endif
%ifarch riscv64
%patch -P2 -p1
%endif

%build
%define _warning_options %{nil}
%cmake -DCMAKE_BUILD_TYPE=Release -DTEST=OFF -DENABLE_EXPORT_API=OFF
%cmake_build

%install
mkdir -p %{buildroot}/%{_prefix}/include
%ifarch sw_64
mkdir -p %{buildroot}/%{_prefix}/lib
%else
mkdir -p %{buildroot}/%{_prefix}/lib64
%endif
cp src/interface/dcf_interface.h %{buildroot}/%{_prefix}/include
%ifarch sw_64
cp output/lib/libdcf.* %{buildroot}/%{_prefix}/lib
%else
cp output/lib/libdcf.* %{buildroot}/%{_prefix}/lib64
%endif

%files
%{_includedir}/dcf_interface.h
%ifarch sw_64
%{_prefix}/lib/libdcf.so
%else
%{_prefix}/lib64/libdcf.so
%endif

%changelog
* Tue Nov 05 2024 Funda Wang <fundawang@yeah.net> - 6.0.0-10
- adopt to cmake macro change

* Sat Nov 2 2024 liuheng <liuheng76@huawei.com> - 6.0.0-9
- Update version to 6.0.0

* Mon Oct 28 2024 Funda Wang <fundawang@yeah.net> - 5.0.1-8
- adopt to cmake macro change

* Mon May 6 2024 liuheng <liuheng76@huawei.com> - 5.0.1-7
- Update version to 5.0.1

* Fri Jul 28 2023 misaka00251 <liuxin@iscas.ac.cn> - 1.0.0-6
- Add riscv64 support

* Mon May 29 2023 huajingyun<huajingyun@loongson.cn> - 1.0.0-5
- add loongarch64 support

* Mon Oct 24 2022 wuzx<wuzx1226@qq.com> - 1.0.0-4
- change lib64 to lib when in sw64 architecture

* Thu Jul 28 2022 wuzx<wuzx1226@qq.com> - 1.0.0-3
- add sw64 patch

* Thu Feb 10 2022 zhangxubo <zhangxubo1@huawei.com> - 1.0.0-2
- #I4T3R3 move library file to /usr/lib64 path.

* Wed Dec 1 2021 zhangxubo <zhangxubo1@huawei.com> - 1.0.0-1
- Package init
